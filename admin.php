
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("db_connection.php");

if(!class_exists("Admin")){
    class Admin{
        public function insert_article($data){

            global $db;
            
            $query = "
                INSERT INTO articles(article_title,article_text,article_price,article_cat) VALUES ('$data[article_title]','$data[article_text]','$data[article_price]','$data[article_cat]')
            ";

            return $db->insert($query);
        }

        public function update_article($data){

            global $db;



            $query = "
            UPDATE articles SET article_title='$data[article_title]', article_text='$data[article_text]', article_price='$data[article_price]' WHERE article_id='$data[article_id]';
            ";

            return $db->update($query);
        }

        public function remove_article($data){
            
            global $db;
           
            $query = "
                DELETE FROM articles WHERE article_id='$data[article_id]'
            ";
            
            return $db->remove($query);
        }

    }
}
?>

<?php
$admin = new Admin;

if(!empty($_POST['btn_insert'])){
 
    global $admin;

    $admin->insert_article($_POST);

}

if(!empty($_POST['btn_update'])){
 
    global $admin;

    $admin->update_article($_POST);

}

if(!empty($_POST['btn_remove'])){
 
    global $admin;

    $admin->remove_article($_POST);

}
?>


<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style/style_admin.css">
        <title>Admin Dashboard</title>
    </head>
    <body>

        <div class="header">
        <h2>Admin Dashboard</h2>
        </div>
        <div class="forms">
            <form method="post" action="admin.php">
                <h3> Insert Article </h3>
                <table>
                <tr>
                        <td>Title: </td>
                        <td><input type="text" name="article_title" class="textInput"></td>
                    </tr>
                    <tr>
                        <td>Text: </td>
                        <td><input type="text" name="article_text" class="textInput"></td>
                    </tr>
                    <tr>
                    <tr>
                        <td>Price: </td>
                        <td><input type="text" name="article_price" class=textInput></td>
                    </tr>
                    <tr>
                        <td>Category: </td>
                        <td><input type="text" name="article_cat" class=textInput></td>
                    </tr>
                        <td></td>
                        <td><input type="submit" name="btn_insert" value="Insert"></td>
                    </tr>
                </table>
            </form>


            <form method="post" action="admin.php">
                <h3> Update Article </h3>
                <table>
                    <tr>
                        <td> Article id: </td>
                        <td><input type="text" name="article_id" class="textInput"></td> 
                    </td>
                <tr>
                        <td>Title: </td>
                        <td><input type="text" name="article_title" class="textInput"></td>
                    </tr>
                    <tr>
                        <td>Text: </td>
                        <td><input type="text" name="article_text" class="textInput"></td>
                    </tr>
                    <tr>
                    <tr>
                        <td>Price: </td>
                        <td><input type="text" name="article_price" class=textInput></td>
                    </tr>
                        <td></td>
                        <td><input type="submit" name="btn_update" value="Update"></td>
                    </tr>
                </table>
            </form>


            <form method="post" action="admin.php">
                <h3> Remove article </h3>
                <table>
                    <tr>
                        <td> Article id: </td>
                        <td><input type="text" name="article_id" class="textInput"></td> 
                    </td>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btn_remove" value="Remove"></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
